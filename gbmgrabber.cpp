// KF
#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/remote_access.h>
#include <KWayland/Client/event_queue.h>
#include <KWayland/Client/registry.h>
#include <KWayland/Client/output.h>
// Qt
#include <QtDebug>
#include <QImage>
// system
#include <fcntl.h>
#include <unistd.h>
// local
#include "gbmgrabber.h"

QAtomicInt GbmGrabber::counter = 0;

static const char * formatGLError(GLenum err)
{
    switch(err) {
    case GL_NO_ERROR:          return "GL_NO_ERROR";
    case GL_INVALID_ENUM:      return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:     return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION";
    case GL_STACK_OVERFLOW:    return "GL_STACK_OVERFLOW";
    case GL_STACK_UNDERFLOW:   return "GL_STACK_UNDERFLOW";
    case GL_OUT_OF_MEMORY:     return "GL_OUT_OF_MEMORY";
    default: return (QLatin1String("0x") + QString::number(err, 16)).toLocal8Bit().constData();
    }
}

GbmGrabber::GbmGrabber(QObject *parent) : QObject(parent)
{
    // open DRM device
    setupDrm();

    // get EGL client extensions
    initClientEglExtensions();

    // initialize EGL context and display
    initEgl();
}

GbmGrabber::~GbmGrabber()
{
    if (m_output)
        m_output->destroy();

    if (m_remoteAccessIface)
        m_remoteAccessIface->destroy();

    if (m_drmFd)
        gbm_device_destroy(m_gbmDevice);

}

void GbmGrabber::setupDrm()
{
    m_drmFd = open("/dev/dri/renderD128", O_RDWR);
    m_gbmDevice = gbm_create_device(m_drmFd);
    if(!m_gbmDevice) {
        qFatal("Cannot create GBM device: %s", strerror(errno));
    }
}

// cloned from KWin AbstractEglBackend
void GbmGrabber::initClientEglExtensions()
{
    // Get the list of client extensions
    const char* clientExtensionsCString = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);
    const QByteArray clientExtensionsString = QByteArray::fromRawData(clientExtensionsCString, qstrlen(clientExtensionsCString));
    if (clientExtensionsString.isEmpty()) {
        // If eglQueryString() returned NULL, the implementation doesn't support
        // EGL_EXT_client_extensions. Expect an EGL_BAD_DISPLAY error.
        qFatal("No client extensions defined! %s", formatGLError(eglGetError()));
    }

    m_egl.extensions = clientExtensionsString.split(' ');
}

void GbmGrabber::initEgl()
{
    // Use eglGetPlatformDisplayEXT() to get the display pointer
    // if the implementation supports it.
    if (!m_egl.extensions.contains(QByteArrayLiteral("EGL_EXT_platform_base")) ||
            !m_egl.extensions.contains(QByteArrayLiteral("EGL_MESA_platform_gbm"))) {
        qFatal("One of required EGL extensions is missing");
    }

    m_egl.display = eglGetPlatformDisplayEXT(EGL_PLATFORM_GBM_MESA, m_gbmDevice, nullptr);
    if (m_egl.display == EGL_NO_DISPLAY) {
        qFatal("Error during obtaining EGL display: %s", formatGLError(eglGetError()));
    }

    EGLint major, minor;
    if (eglInitialize(m_egl.display, &major, &minor) == EGL_FALSE) {
        qFatal("Error during eglInitialize: %s", formatGLError(eglGetError()));
    }

    if (eglBindAPI(EGL_OPENGL_API) == EGL_FALSE) {
        qFatal("bind OpenGL API failed");
    }
    qDebug() << "Egl Initialization succeeded";
    qDebug() << QString("EGL version: %1.%2").arg(major).arg(minor);

    m_egl.context = eglCreateContext(m_egl.display, nullptr, EGL_NO_CONTEXT, nullptr);
    if(m_egl.context == EGL_NO_CONTEXT) {
        qFatal("Couldn't create EGL context: %s", formatGLError(eglGetError()));
    }
}

void GbmGrabber::init(KWayland::Client::ConnectionThread *wlLink, KWayland::Client::EventQueue *queue)
{
    using namespace KWayland::Client;

    // setup registry to discover wayland interfaces
    Registry *registry = new Registry(this);
    registry->setEventQueue(queue);
    registry->create(wlLink);

    // bind output
    // without this KWin will never send us any remote buffers
    connect(registry, &Registry::outputAnnounced, this,
        [this, registry] (quint32 name, quint32 version) {
            qDebug() << "Binding output";

            // we need to bind only once
            if (!m_output) {
                m_output = registry->createOutput(name, version, this);
                connect(m_output, &Output::changed, [=] {
                    qDebug() << "Output bound:" << m_output->manufacturer() << m_output->model();
                });
            }

        }
    );

    // get remote interface when it's announced and grab fds from it
    connect(registry, &Registry::remoteAccessManagerAnnounced, this,
        [this, registry] (quint32 name, quint32 version) {
            qDebug() << "Remote access manager announced, connecting";
            m_remoteAccessIface = registry->createRemoteAccessManager(name, version, this);

            // if buffer is received, wait for its parameters right away, no conversation is needed
            connect(m_remoteAccessIface, &RemoteAccessManager::bufferReady,
                [this] (const void */*output*/, const KWayland::Client::RemoteBuffer *rbuf) {
                    connect(rbuf, &RemoteBuffer::parametersObtained, [this, rbuf] { handleBuffer(rbuf); });
            });
        }
    );

    // this should be the last statement or we can lose events
    registry->setup();
}

void GbmGrabber::handleBuffer(const KWayland::Client::RemoteBuffer *rbuf)
{
    // delete after buffer is handled
    QScopedPointer<const KWayland::Client::RemoteBuffer> guard(rbuf);

    auto gbmHandle = rbuf->fd();
    auto width = rbuf->width();
    auto height = rbuf->height();
    auto stride = rbuf->stride();
    auto format = rbuf->format();

    qDebug() << QString("Incoming GBM fd %1, %2x%3, stride %4, fourcc 0x%5")
        .arg(gbmHandle).arg(width).arg(height).arg(stride).arg(QString::number(format, 16));

    if(!gbm_device_is_format_supported(m_gbmDevice, format, GBM_BO_USE_SCANOUT)) {
        qCritical() << "GBM format is not supported by device!";
        return;
    }

    // import GBM buffer that was passed from KWin
    gbm_import_fd_data importInfo = {gbmHandle, width, height, stride, format};
    gbm_bo *imported = gbm_bo_import(m_gbmDevice, GBM_BO_IMPORT_FD, &importInfo, GBM_BO_USE_SCANOUT);
    if(!imported) {
        qCritical() << "Cannot import passed GBM fd:" << strerror(errno);
        return;
    }

    // bind context to render thread
    eglMakeCurrent(m_egl.display, EGL_NO_SURFACE, EGL_NO_SURFACE, m_egl.context);

    // create EGL image from imported BO
    EGLImageKHR image = eglCreateImageKHR(m_egl.display, NULL, EGL_NATIVE_PIXMAP_KHR, imported, NULL);
    if (image == EGL_NO_IMAGE_KHR) {
        qCritical() << "Error creating EGLImageKHR" << formatGLError(glGetError());
        return;
    }
    // create GL 2D texture for framebuffer
    GLuint texture;
    glGenTextures(1, &texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, texture);
    glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, image);

    // bind framebuffer to copy pixels from
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        qCritical() << "glCheckFramebufferStatus failed:" << formatGLError(glGetError());
        glDeleteTextures(1, &texture);
        glDeleteFramebuffers(1, &framebuffer);
        eglDestroyImageKHR(m_egl.display, image);
        return;
    }

    auto capture = new QImage(QSize(width, height), QImage::Format_RGBA8888);
    glViewport(0, 0, width, height);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, capture->bits());
    capture->save("lastCaptured-" + QString::number(counter.fetchAndAddAcquire(1)) + ".png");
    delete capture;

    // unbind all
    gbm_bo_destroy(imported);
    glDeleteTextures(1, &texture);
    glDeleteFramebuffers(1, &framebuffer);
    eglDestroyImageKHR(m_egl.display, image);

    // from this point buffer object can be safely freed
    close(gbmHandle);
}



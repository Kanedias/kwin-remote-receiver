// Qt
#include <QCoreApplication>
#include <QThread>
#include <QDateTime>
#include <QtDebug>
// KF5
#include <KWayland/Client/connection_thread.h>
#include <KWayland/Client/event_queue.h>
// local
#include "gbmgrabber.h"

void formatLog(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QByteArray timestamp = QDateTime::currentDateTime().toString(Qt::ISODate).toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "[D][%s] (%s): %s \n", context.category, timestamp.constData(), localMsg.constData());
        break;
    case QtInfoMsg:
        fprintf(stderr, "[I][%s] (%s): %s \n", context.category, timestamp.constData(), localMsg.constData());
        break;
    case QtWarningMsg:
        fprintf(stderr, "[W][%s] (%s): %s \n", context.category, timestamp.constData(), localMsg.constData());
        break;
    case QtCriticalMsg:
        fprintf(stderr, "[C!][%s] (%s): %s \n", context.category, timestamp.constData(), localMsg.constData());
        break;
    case QtFatalMsg:
        fprintf(stderr, "[F!!][%s] (%s): %s \n", context.category, timestamp.constData(), localMsg.constData());
        abort();
    }
}

int main(int argc, char* argv[]) {
    using KWayland::Client::ConnectionThread;
    using KWayland::Client::EventQueue;

    // init logging
    qInstallMessageHandler(formatLog);

    // main app
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName(QStringLiteral("KwinRemoteReceiver"));
    QCoreApplication::setApplicationVersion(QStringLiteral("0.1"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("kde.org"));

    qDebug() << "Starting Wayland thread";

    // create background thread for handling wayland events
    auto wlWorker = new QThread(&app);
    auto wlLink = new ConnectionThread(); // confusing name, it's not actually a thread
    wlLink->moveToThread(wlWorker);

    // grabber is main point of operation here, responsible for grabbing and saving
    // images to the working dir
    GbmGrabber *grabber = new GbmGrabber(&app);

    // tricky connection - handover signal to application thread via queued connection
    QObject::connect(wlLink, &ConnectionThread::connected, &app, [&] {
        qDebug() << "Wayland socket connected";
        // now create a queue living in main app thread and set it up
        auto queue = new EventQueue(&app);
        queue->setup(wlLink);

        // and now though wlLink is living in his own thread
        // queue along with registry will be living in main one
        grabber->init(wlLink, queue);
    }, Qt::QueuedConnection);

    QObject::connect(wlLink, &ConnectionThread::errorOccurred, [&] { qDebug() << "Wayland connection error" << wlLink->errorCode(); });
    QObject::connect(wlLink, &ConnectionThread::failed, [&] { qCritical() << "Wayland connection failed"; });
    QObject::connect(wlLink, &ConnectionThread::failed, wlWorker, &QThread::quit);
    QObject::connect(wlLink, &ConnectionThread::connectionDied, [] { qDebug() << "Wayland connection died"; });
    QObject::connect(wlLink, &ConnectionThread::connectionDied, wlWorker, &QThread::quit);

    // kill app when wayland background thread dies
    QObject::connect(wlWorker, &QThread::finished, [] { qDebug() << "Wayland thread finished"; });
    QObject::connect(wlWorker, &QThread::finished, &app, &QCoreApplication::quit);

    wlWorker->start();
    wlLink->initConnection();

    return app.exec();
}

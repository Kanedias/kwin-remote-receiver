#ifndef GBMGRABBER_H
#define GBMGRABBER_H

#include <QObject>
#include <gbm.h>
#include <epoxy/egl.h>
#include <epoxy/gl.h>

namespace KWayland
{
namespace Client
{
class ConnectionThread;
class EventQueue;
class RemoteAccessManager;
class RemoteBuffer;
class Output;
}
}

class GbmGrabber : public QObject
{
    Q_OBJECT
public:
    GbmGrabber(QObject *parent = nullptr);
    virtual ~GbmGrabber();
    void init(KWayland::Client::ConnectionThread *wlLink, KWayland::Client::EventQueue *queue);
private:
    static QAtomicInt counter;

    void setupDrm();
    void initClientEglExtensions();
    void initEgl();
    void handleBuffer(const KWayland::Client::RemoteBuffer *rbuf);

    qint32 m_drmFd = 0; // for GBM buffer mmap
    gbm_device *m_gbmDevice = nullptr; // for passed GBM buffer retrieval
    struct {
        QList<QByteArray> extensions;
        EGLDisplay display = EGL_NO_DISPLAY;
        EGLContext context = EGL_NO_CONTEXT;
    } m_egl;

    KWayland::Client::Output *m_output = nullptr;
    KWayland::Client::RemoteAccessManager *m_remoteAccessIface = nullptr;
};

#endif // GBMGRABBER_H
